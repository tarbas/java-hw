package HW4;

public class Main {


    static String[][] scheduleRod = new String[7][2];

    static void filSch(String[][] scheduleRod) {
        scheduleRod[0][0] = "Sunday";
        scheduleRod[0][1] = "do home work";
        scheduleRod[1][0] = "Monday";
        scheduleRod[1][1] = "go to courses; watch a films";
        scheduleRod[2][0] = "Tuesday";
        scheduleRod[2][1] = "go to courses";
        scheduleRod[3][0] = "Wednesday";
        scheduleRod[3][1] = "go to gym";
        scheduleRod[4][0] = "Thursday";
        scheduleRod[4][1] = "go to courses";
        scheduleRod[5][0] = "Friday";
        scheduleRod[5][1] = "go to dantist";
        scheduleRod[6][0] = "Saturday";
        scheduleRod[6][1] = "have a rest!";
    }


    public static void main(String[] args) {
        filSch(scheduleRod);
        Human johnDow = new Human();
        Human vova = new Human("Volodymyr", "Lozovoi", 54);
        Human natasha = new Human("Natalya", "Lozovaya", 50);
        Pet druzhok = new Pet("dog", "Druzhok", 5, 45, new String[]{"eat", "drink", "sleep"});
        Human rodion = new Human("Rodion", "Lozovoi", 22, natasha, vova, 90, druzhok, scheduleRod);
        System.out.print(rodion);
        rodion.greetPet();
        rodion.describePet();
        druzhok.respond();
        druzhok.eat();
        druzhok.foul();
    }

}
