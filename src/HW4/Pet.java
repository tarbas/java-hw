package HW4;

import java.util.Arrays;

public class Pet {
    String species;
    String nickname;
    int age;
    int trickLevel;
    String[] habits;


    Pet() {

    }

    Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this(species, nickname);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return species + " {" + "nickname=" + nickname +
                ", age=" + age + ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                "}";
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.printf("\nПривет, хозяин. Я - %s. Я соскучился!", nickname);
    }

    public void foul() {
        System.out.println("\nНужно хорошо замести следы...");
    }
}
