package HW4;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] schedule;

    Human() {
    }

    Human(String name, String surname, int year, Human mother, Human father, int iq, Pet pet, String[][] schedule) {
        this(name, surname, year, mother, father);
        this.iq = iq;
        this.pet = pet;
        this.schedule = schedule;
    }

    Human(String name, String surname, int year, Human mother, Human father) {
        this(name, surname, year);
        this.mother = mother;
        this.father = father;
    }

    Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name=" + name +
                ", surname=" + surname +
                ", year=" + year +
                ", iq=" + iq +
                ", mother=" + mother.name + " " + mother.surname +
                ", father=" + father.name + " " + father.surname +
                ", pet= " + pet.toString() +
                '}';
    }


    public void greetPet() {
        System.out.printf("\nПривет, %s", pet.nickname);
    }

    public void describePet() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nУ меня есть ").append(pet.species).append(", ему ").append(pet.age).append(" лет, он у меня ");
        String tricky = pet.trickLevel > 50 ? "очень хитрый." : "почти не хитрый.";
        sb.append(tricky);
        System.out.println(sb);
    }


}
