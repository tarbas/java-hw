package HW3;

import HW4.Main;

import java.util.Scanner;

public class Planner {

    static Scanner in = new Scanner(System.in);
    static int index;
    static String dayForChange;
    static String day;
    static String searchWord1 = "change", searchWord2 = "reschedule";
    static String[][] schedule = new String[7][2];

    static boolean needToChange(String day) {
        boolean result = false;
        if (day.regionMatches(0, searchWord1, 0, 6) ||
                day.regionMatches(0, searchWord2, 0, 6)) result = true;
        return result;
    }

    static void setNewTask() {
        String[] words = day.split(" ");
        for (int j = 0; j < words.length; j++) {
            words[j] = words[j].toLowerCase().trim();
        }

        dayForChange = words[1];
        day = "setNew";

        for (int x = 0; x < schedule.length; x++) {
            if (schedule[x][0].toLowerCase().equals(dayForChange)) index = x;
        }
    }

    public static void main(String[] args) {
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a films";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go to courses";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "go to gym";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "go to courses";
        schedule[5][0] = "Friday";
        schedule[5][1] = "go to dantist";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "have a rest!";

        do {
            System.out.println("Please, input the day of the week:");
            day = in.nextLine();
            day = day.trim().toLowerCase();

            boolean result = needToChange(day);

            if (result) setNewTask();

            switch (day) {
                case ("sunday"):
                    System.out.printf("Your tasks for Sunday: %s\n", schedule[0][1]);
                    break;
                case ("monday"):
                    System.out.printf("Your tasks for Monday: %s\n", schedule[1][1]);
                    break;
                case ("tuesday"):
                    System.out.printf("Your tasks for Tuesday: %s\n", schedule[2][1]);
                    break;
                case ("wednesday"):
                    System.out.printf("Your tasks for Wednesday: %s\n", schedule[3][1]);
                    break;
                case ("thursday"):
                    System.out.printf("Your tasks for Thursday: %s\n", schedule[4][1]);
                    break;
                case ("friday"):
                    System.out.printf("Your tasks for Friday: %s\n", schedule[5][1]);
                    break;
                case ("saturday"):
                    System.out.printf("Your tasks for Saturday: %s\n", schedule[6][1]);
                    break;

                case ("setNew"):
                    System.out.printf("Please, input new tasks for: %s\n", schedule[index][0]);
                    String newTask = in.nextLine();
                    schedule[index][1] = newTask;
                    break;

                default:
                    if (day.equals("exit")) break;
                    System.out.println("Sorry, I don't understand you, please try again");
                    break;
            }

        }
        while (!day.equals("exit"));

    }
}
